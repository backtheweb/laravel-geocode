<?php

namespace Backtheweb\Geocode\Tests\Unit;

use Backtheweb\Geocode\Tests\TestCase;

class PackageTest extends TestCase
{
    /** @test */
    function run_test()
    {

        $geo     = new \Backtheweb\Geocode\Geo(env('GOOGLE_API_KEY'));
        $latLang = $geo->lang('es')->latLng('barcelona', 'ES');

        $this->assertEquals($latLang->lat, 41.3873974);
        $this->assertEquals($latLang->lng, 2.168568);
    }
}
