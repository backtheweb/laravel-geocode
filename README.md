# geo

## Installation

Install the package via composer:

```bash
$ composer require backtheweb/laravel-geocode:dev-main
```

Publish the config file:

```bash
$ php artisan vendor:publish --provider="Backtheweb\\Geocode\\GeocodeServiceProvider" --tag="config"
```

To start developing, run composer install on the package

```bash
$ cd {{ path }}
$ composer install
```

## Run tests

Before running the tests, add the following to phpunit.xml file:

```xml
<env name="GOOGLE_API_KEY" value="YOUR_API_KEY"/>
```
replace YOUR_API_KEY with your Google API key.

Then run the tests:

```bash
$ ./vendor/bin/phpunit
```
