<?php

return [
    'google' => [
        'developerKey' => env('GOOGLE_API_KEY'),
    ]
];
