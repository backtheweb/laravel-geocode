<?php

namespace Backtheweb\Geocode;


use GuzzleHttp\Exception\GuzzleException;
use Backtheweb\Geocode\Exception\InvalidRequest;
use Backtheweb\Geocode\Exception\OverQueryLimit;
use Backtheweb\Geocode\Exception\RequestDenied;
use Backtheweb\Geocode\Exception\ResponseError;
use Backtheweb\Geocode\Exception\UnknownError;
use Backtheweb\Geocode\Exception\ZeroResults;

class Geo extends GoogleClientAbstract
{
    protected string $base_uri = 'https://maps.googleapis.com/maps/api/geocode/json';

    /** @var bool  */
    public bool $sensor = false;

    public array $components = [];

    /**
     * @throws OverQueryLimit
     * @throws RequestDenied
     * @throws ZeroResults
     * @throws Exception
     * @throws InvalidRequest
     * @throws UnknownError
     * @throws GuzzleException
     * @throws ResponseError
     */
    public function get($address, $region = null): mixed
    {
        $params = [
            'address' => $address,
            'region'  => $region,
        ];

        return $this->call($params);
    }
    public function latLng($address, $region = null): null|object
    {
        $params = [
            'address' => $address,
            'region'  => $region,
        ];

        $response = $this->call($params);

        if(!isset($response->results[0])){
            return null;
        }
        return $response->results[0]?->geometry?->location;
    }

    public function sensor(bool $bool = null):string
    {
        if($bool !== null){
            $this->sensor = $bool;
        }

        return $this->sensor;
    }
}
