<?php

namespace Backtheweb\Geocode\Exception;

use Backtheweb\Geocode\Exception;

class RequestDenied extends Exception
{
    protected $message = 'Google maps api: The provided API key is invalid';
}
