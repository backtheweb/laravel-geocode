<?php

namespace Backtheweb\Geocode\Exception;

use Backtheweb\Geocode\Exception;

class OverQueryLimit extends Exception
{
    protected $message = 'Google maps api: Over query limit';
}
