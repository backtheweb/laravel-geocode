<?php

namespace Backtheweb\Geocode\Exception;

use Backtheweb\Geocode\Exception;

class ZeroResults extends Exception
{
    protected $message = 'Google maps api: Zero errors';
}
