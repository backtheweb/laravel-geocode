<?php

namespace Backtheweb\Geocode\Exception;

use Backtheweb\Geocode\Exception;

class InvalidRequest extends Exception
{
    protected $message = 'Google maps api: Invalid request';
}
