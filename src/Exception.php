<?php

namespace Backtheweb\Geocode;

class Exception extends \Exception
{
    protected $message = 'Geo error';
}
