<?php

namespace Backtheweb\Geocode\Console;

use Illuminate\Console\Command;
use Backtheweb\Geocode\Facades\Geo;

class GeoCommand extends Command
{
    protected $signature = 'geo {address} {-r|--region=}';

    protected $description = 'geocode address';

    public function handle() : int
    {
        $address = $this->argument('address');
        $region  = $this->option('region');
        $latLng  = Geo::lang('es')->latLng($address, $region);

        $this->info($latLng->lat . "," . $latLng->lng);

        return Command::SUCCESS;
    }
}
