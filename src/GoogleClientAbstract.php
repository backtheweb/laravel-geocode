<?php

namespace Backtheweb\Geocode;

use GuzzleHttp\Client;

use GuzzleHttp\Exception\GuzzleException;
use Backtheweb\Geocode\Exception\InvalidRequest;
use Backtheweb\Geocode\Exception\OverQueryLimit;
use Backtheweb\Geocode\Exception\RequestDenied;
use Backtheweb\Geocode\Exception\ResponseError;
use Backtheweb\Geocode\Exception\UnknownError;
use Backtheweb\Geocode\Exception\ZeroResults;

/**
 * Class ServiceAbstract
 * @package N\Geo\Service\Google
 */
abstract class GoogleClientAbstract
{
    const STATUS_OK                 = 'OK';
    const STATUS_ZERO_RESULTS       = 'ZERO_RESULTS';
    const STATUS_OVER_QUERY_LIMIT   = 'OVER_QUERY_LIMIT';
    const STATUS_INVALID_REQUEST    = 'INVALID_REQUEST';
    const STATUS_UNKNOWN_ERROR      = 'UNKNOWN_ERROR';
    const STATUS_RESPONSE_ERROR     = 'RESPONSE_ERROR';
    const REQUEST_DENIED            = 'REQUEST_DENIED';
    private Client $client;

    protected string $base_uri;

    protected string $url = '';

    protected float $timeout  = 2.0;

    /**
     * @param string $api_ley
     * @param string $lang
     */
    public function __construct( protected string $api_key, protected string $lang = 'en')
    {
        $this->client = new Client([
            'base_uri' => $this->base_uri,
            'timeout'  => $this->timeout,
        ]);
    }

    /**
     * @throws OverQueryLimit
     * @throws RequestDenied
     * @throws ZeroResults
     * @throws InvalidRequest
     * @throws UnknownError
     * @throws Exception
     * @throws GuzzleException
     * @throws ResponseError
     */
    public function call(array $params = [] )
    {
        $params = array_merge( [
            'key'       => $this->api_key,
            'language'  => $this->lang,
            'timestamp' => time(),
        ], $params);

        /** @var \GuzzleHttp\Psr7\Response $response */
        $response = $this->client->request('GET', $this->url, [
            'query' => $params,
        ]);


        if($response->getStatusCode() !== 200){
            $this->throwErrorResponse($response);
        }

        return json_decode($response->getBody()->getContents());
    }

    /**
     * @throws OverQueryLimit
     * @throws RequestDenied
     * @throws ZeroResults
     * @throws InvalidRequest
     * @throws Exception
     * @throws UnknownError
     * @throws ResponseError
     */
    private function throwErrorResponse($response)
    {
        throw match ($response->status) {
            self::STATUS_ZERO_RESULTS     => new ZeroResults(),
            self::STATUS_OVER_QUERY_LIMIT => new OverQueryLimit(),
            self::STATUS_INVALID_REQUEST  => new InvalidRequest(),
            self::STATUS_UNKNOWN_ERROR    => new UnknownError(),
            self::STATUS_RESPONSE_ERROR   => new ResponseError(),
            self::REQUEST_DENIED          => new RequestDenied(),
            default                       => new Exception($response->message),
        };
    }

    /**
     * Get or Set the lang
     * @param string|null $lang
     * @return self|string
     */
    public function lang(string $lang = null):self|string
    {
        if($lang !== null){
            $this->lang = $lang;
            return $this;
        }

        return $this->lang;
    }
}
