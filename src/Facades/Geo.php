<?php

namespace Backtheweb\Geocode\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static \Backtheweb\Geocode\Geo lang(string $lang = null):string
 * @method static \Backtheweb\Geocode\Geo sensor(bool $senor = null):bool
 * @method static \Backtheweb\Geocode\Geo get($address, $region = null):mixed
 * @method static \Backtheweb\Geocode\Geo latLng($address, $region = null):object|null
 */
class Geo extends Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'geo';
    }
}
