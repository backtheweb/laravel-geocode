<?php

namespace Backtheweb\Geocode;

use Illuminate\Support\ServiceProvider;
use Backtheweb\Geocode\Console\GeoCommand;

class GeocodeServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/geocode.php', 'geocode');

        $this->app->bind('geo', function($app) {

            return new Geo(config('geocode.google.developerKey'));
        });
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                GeoCommand::class,
            ]);

            $this->publishes([
                __DIR__.'/../config/geocode.php' => config_path('geocode.php'),
            ], 'config');
        }
    }

    public function provides()
    {
        return [
            'geo',
        ];
    }
}
